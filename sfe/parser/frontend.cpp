#include <iostream>
#include <fstream>

#include "frontend.h"
#include "parser.h"

tree frontendParseFile(const char * fileName) {
    std::ifstream file(fileName);
    if (!file.is_open()) {
        std::cout << "Error: Unable to open input file!" << std::endl;
        return NULL;
    }

    tree res;

    try {
        Parser parser(file);
        res = parser.parse();
        std::cout << "Parsing done." << std::endl;
    } catch (ParserException & ex) {
        std::cout << "Parsing terminated after error!" << std::endl;
        file.close();
        exit(1);
    }

    file.close();

    return res;
}
