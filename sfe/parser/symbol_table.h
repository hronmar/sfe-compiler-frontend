#ifndef SYMBOL_TABLE_H
#define SYMBOL_TABLE_H

#include <map>
#include <string>
#include <vector>

#include "../sfe-lang.h"

enum SymbolType {
    UNDEF, VARIABLE, CONSTANT, ARRAY, FUNCTION, PROCEDURE
};

struct Symbol {
    SymbolType m_SymbolType;
    tree m_TreeNode;
    bool m_External;

    Symbol(SymbolType symbolType, tree treeNode) :
    m_SymbolType(symbolType), m_TreeNode(treeNode), m_External(false) {
    }

    Symbol(SymbolType symbolType, tree treeNode, bool external) :
    m_SymbolType(symbolType), m_TreeNode(treeNode), m_External(external) {
    }

};

/* symbol table singleton */
class SymbolTable {
public:

    static SymbolTable & getInstance() {
        static SymbolTable instance;
        return instance;
    }

    ~SymbolTable();

    /* symbol declaration */
    void declareVariable(const std::string & ident);
    void declareConstant(const std::string & ident, int value);
    void declareArray(const std::string & ident, int lowerBound, int upperBound);

    /* function declaration */

    /* declare function header */
    void startFunctionDecl(const std::string & ident,
            const std::vector<std::string> & paramsList, bool isProcedure, bool forwardDecl = false);

    /* declare function block and its variables */
    void functionVarsDecl();

    /* declare function body, finish declaration */
    void finishFunctionDecl(tree body_stmts);

    /* returns whether function is being declare */
    bool isFunctionDecl() const;
    /* returns currently declared function */
    tree getCurrentFunction() const;

    /* returns symbol with this name, NULL if it does not exist */
    Symbol * getSymbol(const std::string & ident, bool hideLocal = false);

private:

    std::map<std::string, Symbol*> m_SymbolTable;
    std::map<std::string, Symbol*> m_LocalSymbols;

    bool m_FunctionDecl;
    bool m_IsProcedure;
    std::string m_FunctionIdent;

    tree func_decl;
    tree var_decls;
    tree block;

    SymbolTable() : m_FunctionDecl(false) {
    }

    SymbolTable(const SymbolTable &) /* = delete */;
    void operator=(const SymbolTable &) /* = delete */;
};

#endif /* SYMBOL_TABLE_H */
