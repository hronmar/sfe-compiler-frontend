#include <iostream>
#include <vector>

#include "parser.h"
#include "ast_nodes.h"
#include "symbol_table.h"

Parser::Parser(std::istream & input) : m_Lexan(input) {
    m_Symbol = m_Lexan.nextToken();
}

Parser::~Parser() {
}

tree Parser::parse() {
    //call starting nonterminal
    Node * node = nonterm_Prog();
    tree res;

    //translate
    res = node->translate();

    delete node;

    return res;
}

Token Parser::match(TokenType t) {
    Token token = m_Symbol;
    m_Symbol = m_Lexan.nextToken();

    if (m_Symbol.m_Type == ERROR) {
        //error from lexan
        std::cout << "on line: " << m_Lexan.getCurrentLine() << std::endl;
        throw ParserException();
    }

    if (token.m_Type != t) {
        //wrong token type
        std::cout << "Parser error: " << Token::tokenTypesNames[t] << " expected!" << std::endl;
        std::cout << "on line: " << m_Lexan.getCurrentLine() << std::endl;
        throw ParserException();
    }

    return token;
}

Prog * Parser::nonterm_Prog() {
    match(KW_PROGRAM);
    match(IDENT);
    match(SEMICOLON);

    //procedures / functions declarations
    nonterm_FuncDecl();

    //main block
    nonterm_Decl();
    Prog * prg = new Prog(nonterm_StmtBlock(true));

    match(DOT);

    return prg;
}

//function declarations

void Parser::nonterm_FuncDecl() {
    switch (m_Symbol.m_Type) {
        case KW_FUNCTION:
        case KW_PROCEDURE:
        {
            //function / procedure declaration
            TokenType type = m_Symbol.m_Type;
            match(type);
            std::string funcName = match(IDENT).m_StrVal;

            Symbol * funcSymb = SymbolTable::getInstance().getSymbol(funcName);
            if (funcSymb != NULL && !funcSymb->m_External) {
                //function redeclaration
                std::cout << "Error: redeclaration of function " << funcName << std::endl;
                std::cout << "on line: " << m_Lexan.getCurrentLine() << std::endl;
                throw ParserException();
            }

            match(L_PAR);
            //parameters
            std::vector<std::string> paramsList;
            if (m_Symbol.m_Type != R_PAR)
                nonterm_FuncParamDecl(paramsList);
            match(R_PAR);

            if (type == KW_FUNCTION) {
                //function return type
                match(COLON);
                Token type = match(IDENT);
                checkType(type);
            }
            match(SEMICOLON);

            if (m_Symbol.m_Type == KW_FORWARD) {
                //forward declaration
                match(KW_FORWARD);
                match(SEMICOLON);
                SymbolTable::getInstance().startFunctionDecl(funcName, paramsList, (type == KW_PROCEDURE), true);
                nonterm_FuncDecl();
                break;
            }

            SymbolTable::getInstance().startFunctionDecl(funcName, paramsList, (type == KW_PROCEDURE));

            nonterm_Decl();
            SymbolTable::getInstance().functionVarsDecl();

            StmtList * block = nonterm_StmtBlock(true);
            SymbolTable::getInstance().finishFunctionDecl(block->translate());
            delete block;

            match(SEMICOLON);

            nonterm_FuncDecl();
            break;
        }

        default:
            ;
    }
}

void Parser::nonterm_FuncParamDecl(std::vector<std::string> & paramsList) {
    switch (m_Symbol.m_Type) {
        case IDENT:
        {
            std::string paramName = match(IDENT).m_StrVal;
            paramsList.push_back(paramName);
            match(COLON);
            Token type = match(IDENT);
            checkType(type);
            if (m_Symbol.m_Type == SEMICOLON) {
                match(SEMICOLON);
                nonterm_FuncParamDecl(paramsList);
            }
        }
        default:
            ;
    }
}

//declarations

void Parser::nonterm_Decl() {
    //constant and variable declarations
    switch (m_Symbol.m_Type) {
        case KW_VAR:
            match(KW_VAR);
            nonterm_VarDeclBlock();
            nonterm_Decl();
            break;
        case KW_CONST:
            match(KW_CONST);
            nonterm_ConstDecl();
            nonterm_Decl();
            break;
        default:
            ;
    }
}

void Parser::nonterm_VarDeclBlock() {
    switch (m_Symbol.m_Type) {
        case IDENT:
            nonterm_VarDecl();
            match(SEMICOLON);
            nonterm_VarDeclBlock();
            break;
        default:
            ;
    }
}

void Parser::nonterm_VarDecl() {
    std::vector<std::string> symbolList;
    Token symb = match(IDENT);
    symbolList.push_back(symb.m_StrVal);
    nonterm_VarDecl2(symbolList);
    match(COLON);
    nonterm_DeclType(symbolList);
}

void Parser::nonterm_VarDecl2(std::vector<std::string> & symbolList) {
    switch (m_Symbol.m_Type) {
        case COMMA:
        {
            match(COMMA);
            Token symb = match(IDENT);
            symbolList.push_back(symb.m_StrVal);
            nonterm_VarDecl2(symbolList);
            break;
        }
        default:
            ;
    }
}

void Parser::nonterm_ConstDecl() {
    switch (m_Symbol.m_Type) {
        case IDENT:
        {
            Token symb = match(IDENT);
            match(EQUAL);
            Token val = match(NUMBER);
            match(SEMICOLON);
            if (SymbolTable::getInstance().getSymbol(symb.m_StrVal) != NULL) {
                //redeclaration
                std::cout << "Error: redeclaration of symbol " << symb.m_StrVal << std::endl;
                std::cout << "on line: " << m_Lexan.getCurrentLine() << std::endl;
                throw ParserException();
            }
            SymbolTable::getInstance().declareConstant(symb.m_StrVal, val.m_NumVal);
            nonterm_ConstDecl();
            break;
        }
        default:
            ;
    }
}

void Parser::nonterm_DeclType(std::vector<std::string> & symbolList) {
    switch (m_Symbol.m_Type) {
        case IDENT:
        {
            Token type = match(IDENT);
            checkType(type);

            for (size_t i = 0; i < symbolList.size(); i++) {
                if (SymbolTable::getInstance().getSymbol(symbolList[i]) != NULL) {
                    //redeclaration
                    std::cout << "Error: redeclaration of symbol " << symbolList[i] << std::endl;
                    std::cout << "on line: " << m_Lexan.getCurrentLine() << std::endl;
                    throw ParserException();
                }

                SymbolTable::getInstance().declareVariable(symbolList[i]);
            }
            break;
        }
        case KW_ARRAY:
        {
            match(KW_ARRAY);
            match(L_BRACKET);
            int lowerBound = 1;
            if (m_Symbol.m_Type == MINUS) {
                //unary minus
                match(MINUS);
                lowerBound = -1;
            }
            lowerBound *= match(NUMBER).m_NumVal;
            match(DOT);
            match(DOT);
            int upperBound = 1;
            if (m_Symbol.m_Type == MINUS) {
                match(MINUS);
                upperBound = -1;
            }
            upperBound *= match(NUMBER).m_NumVal;
            match(R_BRACKET);
            match(KW_OF);
            Token type = match(IDENT);
            checkType(type);

            for (size_t i = 0; i < symbolList.size(); i++) {
                if (SymbolTable::getInstance().getSymbol(symbolList[i]) != NULL) {
                    //redeclaration
                    std::cout << "Error: redeclaration of symbol " << symbolList[i] << std::endl;
                    std::cout << "on line: " << m_Lexan.getCurrentLine() << std::endl;
                    throw ParserException();
                }

                SymbolTable::getInstance().declareArray(symbolList[i], lowerBound, upperBound);
            }
            break;
        }
        default:
            std::cout << "Parser error: type expected!" << std::endl;
            std::cout << "on line: " << m_Lexan.getCurrentLine() << std::endl;
            throw ParserException();
    }
}

//statements

StmtList * Parser::nonterm_StmtBlock(bool functionBlock/* = false*/) {
    if (functionBlock || m_Symbol.m_Type == KW_BEGIN) {
        match(KW_BEGIN);
        StmtList * stmts = nonterm_StmtBlock2();
        match(KW_END);
        return stmts;
    }

    //if it is not function block, it may consist of only 1 statement
    //then it may not be within KW_BEGIN and KW_END keywords
    return new StmtList(nonterm_Stmt(), NULL);
}

StmtList * Parser::nonterm_StmtBlock2() {
    switch (m_Symbol.m_Type) {
        case KW_END:
            return NULL;
        case KW_BEGIN:
        {
            //inner block
            StmtList * block = nonterm_StmtBlock();
            block->appendStmtList(nonterm_StmtBlock2());
            return block;
        }
        default:
        {
            Stmt * stmt = nonterm_Stmt();
            //if it is not end of block, there should be semicolon
            if (m_Symbol.m_Type != KW_END)
                match(SEMICOLON);
            return new StmtList(stmt, nonterm_StmtBlock2());
        }
    }
}

Stmt * Parser::nonterm_Stmt() {
    switch (m_Symbol.m_Type) {
        case IDENT:
        case KW_DEC:
        case KW_INC:
        {
            Expr * left = nonterm_Atom(false);
            if (m_Symbol.m_Type == ASSIGN) {
                //assignment
                return nonterm_AssignStmt(left);
            } else {
                //expression with side effect
                return new ExprStmt(left);
            }
        }
        case KW_READLN:
        {
            //readln
            match(KW_READLN);
            match(L_PAR);
            Expr * lvalue = nonterm_Atom();
            if (!lvalue->isLValue()) {
                std::cout << "Error: lvalue expected as argument of readln!" << std::endl;
                std::cout << "on line: " << m_Lexan.getCurrentLine() << std::endl;
                throw ParserException();
            }
            match(R_PAR);
            return new ReadLn(lvalue);
        }
        case KW_WRITELN:
        {
            //writeln
            match(KW_WRITELN);
            match(L_PAR);
            Expr * e = nonterm_Expr();
            match(R_PAR);
            return new WriteLn(e);
        }
        case KW_IF:
        {
            //if statement
            match(KW_IF);
            Expr * cond = nonterm_Expr();
            match(KW_THEN);
            StmtList * thenBlock = nonterm_StmtBlock();
            StmtList * elseBlock;

            if (m_Symbol.m_Type == KW_ELSE) {
                //else block is optional
                match(KW_ELSE);
                elseBlock = nonterm_StmtBlock();
            } else {
                elseBlock = new StmtList(new EmptyStmt(), NULL);
            }

            return new IfStmt(cond, thenBlock, elseBlock);
        }
        case KW_WHILE:
        {
            //while loop
            match(KW_WHILE);
            Expr * cond = nonterm_Expr();
            match(KW_DO);
            StmtList * block = nonterm_StmtBlock();
            return new WhileStmt(cond, block);
        }
        case KW_FOR:
        {
            //for loop
            match(KW_FOR);
            //get variable, will be handled inside nonterm_AssignStmt if it is invalid
            Symbol * var = SymbolTable::getInstance().getSymbol(m_Symbol.m_StrVal);
            Stmt * init = nonterm_AssignStmt(nonterm_Atom());
            TokenType type = m_Symbol.m_Type;
            if (type != KW_TO && type != KW_DOWNTO) {
                std::cout << "Error: to or downto keyword expected but got "
                        << Token::tokenTypesNames[type] << std::endl;
                std::cout << "on line: " << m_Lexan.getCurrentLine() << std::endl;
                throw ParserException();
            }
            match(type);
            Expr * to = nonterm_Expr();
            match(KW_DO);
            StmtList * block = nonterm_StmtBlock();
            return new ForStmt(type, var, init, to, block);
        }
        case KW_BREAK:
            match(KW_BREAK);
            return new BreakStmt();
        case SEMICOLON:
            //empty statement
            return new EmptyStmt();
        case KW_EXIT:
            //exit inside function block
            match(KW_EXIT);
            if (!SymbolTable::getInstance().isFunctionDecl()) {
                std::cout << "Error: exit statement out of function block!" << std::endl;
                std::cout << "on line: " << m_Lexan.getCurrentLine() << std::endl;
                throw ParserException();
            }
            return new ExitStmt(SymbolTable::getInstance().getCurrentFunction());
        default:
            //error handling (unexpected token)
            std::cout << "Parser error: unexpected "
                    << Token::tokenTypesNames[m_Symbol.m_Type] << " token!" << std::endl;
            std::cout << "on line: " << m_Lexan.getCurrentLine() << std::endl;
            std::cout << "Note: statement expected." << std::endl;
            throw ParserException();
    }
}

Stmt * Parser::nonterm_AssignStmt(Expr * lvalue) {
    if (!lvalue->isLValue()) {
        std::cout << "Error: lvalue expected on left side of assignment!" << std::endl;
        std::cout << "on line: " << m_Lexan.getCurrentLine() << std::endl;
        throw ParserException();
    }
    match(ASSIGN);
    return new Assign(lvalue, nonterm_Expr());
}

//and, or logical expressions

Expr * Parser::nonterm_Expr() {
    return nonterm_Expr2(nonterm_LogTerm());
}

Expr * Parser::nonterm_Expr2(Expr * c) {
    switch (m_Symbol.m_Type) {
        case KW_OR:
            match(KW_OR);
            return nonterm_Expr2(new CondOr(c, nonterm_LogTerm()));
        case KW_AND:
            match(KW_AND);
            return nonterm_Expr2(new CondAnd(c, nonterm_LogTerm()));
        default:
            return c;
    }
}

//relational expressions

Expr * Parser::nonterm_LogTerm() {
    return nonterm_LogTerm2(nonterm_AritExpr());
}

Expr * Parser::nonterm_LogTerm2(Expr * c) {
    switch (m_Symbol.m_Type) {
        case LESS:
        case LESS_OR_EQ:
        case GREATER:
        case GREATER_OR_EQ:
        case EQUAL:
        case NOT_EQUAL:
        {
            TokenType opType = m_Symbol.m_Type;
            match(opType);
            return nonterm_LogTerm2(new CondRelOp(opType, c, nonterm_AritExpr()));
        }
        default:
            return c;
    }
}

//arithmetic axpressions

Expr * Parser::nonterm_AritExpr() {
    switch (m_Symbol.m_Type) {
        case MINUS:
            match(MINUS);
            return nonterm_AritExpr2(new UnMinus(nonterm_T()));
        case NUMBER:
        case IDENT:
        case KW_DEC:
        case KW_INC:
        case L_PAR:
        case KW_TRUE:
        case KW_FALSE:
            return nonterm_AritExpr2(nonterm_T());
            break;
        default:
            std::cout << "Parser error: unexpected "
                    << Token::tokenTypesNames[m_Symbol.m_Type] << " token!" << std::endl;
            std::cout << "on line: " << m_Lexan.getCurrentLine() << std::endl;
            std::cout << "Note: minus, number, identifier or '(' expected." << std::endl;
            throw ParserException();
    }
}

Expr * Parser::nonterm_AritExpr2(Expr * e) {
    switch (m_Symbol.m_Type) {
        case PLUS:
            match(PLUS);
            return nonterm_AritExpr2(new BinPlus(e, nonterm_T()));
        case MINUS:
            match(MINUS);
            return nonterm_AritExpr2(new BinMinus(e, nonterm_T()));
        default:
            return e;
    }
}

Expr * Parser::nonterm_T() {
    switch (m_Symbol.m_Type) {
        case NUMBER:
        case IDENT:
        case KW_DEC:
        case KW_INC:
        case L_PAR:
        case KW_TRUE:
        case KW_FALSE:
            return nonterm_T2(nonterm_F());
        default:
            std::cout << "Parser error: unexpected "
                    << Token::tokenTypesNames[m_Symbol.m_Type] << " token!" << std::endl;
            std::cout << "on line: " << m_Lexan.getCurrentLine() << std::endl;
            std::cout << "Note: number, identifier or '(' expected." << std::endl;
            throw ParserException();
    }
}

Expr * Parser::nonterm_T2(Expr * e) {
    switch (m_Symbol.m_Type) {
        case TIMES:
            match(TIMES);
            return nonterm_T2(new BinMultiply(e, nonterm_F()));
        case DIVIDED:
            match(DIVIDED);
            return nonterm_T2(new BinDivide(e, nonterm_F()));
        case MOD:
            match(MOD);
            return nonterm_T2(new BinMod(e, nonterm_F()));

        default:
            return e;
    }
}

Expr * Parser::nonterm_F() {
    switch (m_Symbol.m_Type) {
        case NUMBER:
        case IDENT:
        case KW_DEC:
        case KW_INC:
        case KW_TRUE:
        case KW_FALSE:
            return nonterm_Atom();
        case L_PAR:
        {
            match(L_PAR);
            Expr * e = nonterm_Expr();
            match(R_PAR);
            return e;
        }
        default:
            std::cout << "Parser error: unexpected "
                    << Token::tokenTypesNames[m_Symbol.m_Type] << " token!" << std::endl;
            std::cout << "on line: " << m_Lexan.getCurrentLine() << std::endl;
            std::cout << "Note: number, identifier or '(' expected." << std::endl;
            throw ParserException();
    }
}

Expr * Parser::nonterm_Atom(bool rvalue/* = true*/) {
    switch (m_Symbol.m_Type) {
        case NUMBER:
        {
            Token t = match(NUMBER);
            return new NumLiteral(t.m_NumVal);
        }
        case KW_TRUE:
            match(KW_TRUE);
            return new CondTrue();
        case KW_FALSE:
            match(KW_FALSE);
            return new CondFalse();
        case KW_DEC:
        {
            //decrement
            match(KW_DEC);
            match(L_PAR);
            Expr * lvalue = nonterm_Atom();
            if (!lvalue->isLValue()) {
                std::cout << "Error: lvalue expected as argument of dec()!" << std::endl;
                std::cout << "on line: " << m_Lexan.getCurrentLine() << std::endl;
                throw ParserException();
            }
            match(R_PAR);

            return new PreDecrement(lvalue);
        }
        case KW_INC:
        {
            //increment
            match(KW_INC);
            match(L_PAR);
            Expr * lvalue = nonterm_Atom();
            if (!lvalue->isLValue()) {
                std::cout << "Error: lvalue expected as argument of inc()!" << std::endl;
                std::cout << "on line: " << m_Lexan.getCurrentLine() << std::endl;
                throw ParserException();
            }
            match(R_PAR);

            return new PreIncrement(lvalue);
        }
        case IDENT:
        {
            Token t = match(IDENT);

            Symbol * symb = SymbolTable::getInstance().getSymbol(t.m_StrVal);
            if (symb == NULL) {
                std::cout << "Error: undeclared symbol used in expression: " << t.m_StrVal << std::endl;
                std::cout << "on line: " << m_Lexan.getCurrentLine() << std::endl;
                throw ParserException();
            }

            if (m_Symbol.m_Type == L_BRACKET) {
                //array reference
                match(L_BRACKET);
                Expr * index = nonterm_Expr();
                match(R_BRACKET);
                if (symb->m_SymbolType != ARRAY) {
                    std::cout << "Error: array expected: " << t.m_StrVal << std::endl;
                    std::cout << "on line: " << m_Lexan.getCurrentLine() << std::endl;
                    throw ParserException();
                }
                return new ArrayRef(symb, index);
            } else if (m_Symbol.m_Type == L_PAR) {
                //we dont have local functions, so search only global symbols
                symb = SymbolTable::getInstance().getSymbol(t.m_StrVal, true);
                //function call
                if (symb == NULL || (symb->m_SymbolType != FUNCTION && symb->m_SymbolType != PROCEDURE)) {
                    std::cout << "Error: function or procedure expected: " << t.m_StrVal << std::endl;
                    std::cout << "on line: " << m_Lexan.getCurrentLine() << std::endl;
                    throw ParserException();
                }

                if (symb->m_SymbolType == PROCEDURE && rvalue) {
                    std::cout << "Error: procedure call cannot be used as r-value: " << t.m_StrVal << std::endl;
                    std::cout << "on line: " << m_Lexan.getCurrentLine() << std::endl;
                    throw ParserException();
                }

                return nonterm_FuncCall(symb);
            }

            //variable or constant
            if (symb->m_SymbolType != VARIABLE && symb->m_SymbolType != CONSTANT) {
                std::cout << "Error: variable or constant expected: " << t.m_StrVal << std::endl;
                std::cout << "on line: " << m_Lexan.getCurrentLine() << std::endl;
                throw ParserException();
            }
            return new Variable(symb);
        }
        default:
            std::cout << "Parser error: unexpected "
                    << Token::tokenTypesNames[m_Symbol.m_Type] << " token!" << std::endl;
            std::cout << "on line: " << m_Lexan.getCurrentLine() << std::endl;
            throw ParserException();
    }
}

//function call

Expr * Parser::nonterm_FuncCall(Symbol * symb) {
    match(L_PAR);
    std::vector<Expr *> argsList;
    if (m_Symbol.m_Type != R_PAR)
        nonterm_FuncCallArgs(argsList);
    match(R_PAR);
    return new FuncCall(symb, argsList);
}

//arguments

void Parser::nonterm_FuncCallArgs(std::vector<Expr *> & argsList) {
    Expr * arg = nonterm_Expr();
    argsList.push_back(arg);

    if (m_Symbol.m_Type == COMMA) {
        match(COMMA);
        nonterm_FuncCallArgs(argsList);
    }
}

void Parser::checkType(const Token & type) {
    //only integer type is supported
    if (type.m_StrVal != "integer") {
        std::cout << "Error: invalid type: " << type.m_StrVal << std::endl;
        std::cout << "on line: " << m_Lexan.getCurrentLine() << std::endl;
        throw ParserException();
    }
}
