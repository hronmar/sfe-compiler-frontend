#include "symbol_table.h"

SymbolTable::~SymbolTable() {
    for (std::map<std::string, Symbol*>::iterator it = m_SymbolTable.begin(); it != m_SymbolTable.end(); it++)
        delete it->second;
}

void SymbolTable::declareVariable(const std::string & ident) {
    tree var = build_decl(UNKNOWN_LOCATION, VAR_DECL, get_identifier(ident.c_str()), integer_type_node);
    TREE_ADDRESSABLE(var) = true;
    TREE_USED(var) = true;

    if (!m_FunctionDecl) {
        //global declaration
        TREE_STATIC(var) = true;
        TREE_PUBLIC(var) = true;

        DECL_INITIAL(var) = build_int_cst(integer_type_node, 0);

        register_global_variable_declaration(var);
        m_SymbolTable[ident] = new Symbol(VARIABLE, var);
    } else {
        //local variable in function
        var_decls = chainon(var_decls, var);
        m_LocalSymbols[ident] = new Symbol(VARIABLE, var);
    }
}

void SymbolTable::declareConstant(const std::string & ident, int value) {
    if (!m_FunctionDecl) {
        //global declaration
        m_SymbolTable[ident] = new Symbol(CONSTANT, build_int_cst(integer_type_node, value));
    } else {
        //local constant in function
        m_LocalSymbols[ident] = new Symbol(CONSTANT, build_int_cst(integer_type_node, value));
    }
}

void SymbolTable::declareArray(const std::string & ident, int lowerBound, int upperBound) {
    tree index_type = build_range_type(integer_type_node,
            build_int_cst(integer_type_node, lowerBound), build_int_cst(integer_type_node, upperBound));
    tree array_type = build_array_type(integer_type_node, index_type);
    tree array = build_decl(UNKNOWN_LOCATION, VAR_DECL, get_identifier(ident.c_str()), array_type);
    TREE_ADDRESSABLE(array) = true;
    TREE_USED(array) = true;

    if (!m_FunctionDecl) {
        //global declaration
        TREE_STATIC(array) = true;
        TREE_PUBLIC(array) = true;

        DECL_INITIAL(array) = build_int_cst(integer_type_node, 0);

        register_global_variable_declaration(array);
        m_SymbolTable[ident] = new Symbol(ARRAY, array);
    } else {
        //local variable in function
        var_decls = chainon(var_decls, array);
        m_LocalSymbols[ident] = new Symbol(ARRAY, array);
    }
}

void SymbolTable::startFunctionDecl(const std::string & ident,
        const std::vector<std::string> & paramsList, bool isProcedure, bool forwardDecl/* = false*/) {
    m_FunctionDecl = true;
    m_IsProcedure = isProcedure;
    m_FunctionIdent = ident;

    tree params = NULL_TREE;
    params = chainon(params, tree_cons(NULL_TREE, integer_type_node, NULL_TREE));

    tree res_decl = build_decl(BUILTINS_LOCATION, RESULT_DECL, NULL_TREE, integer_type_node);
    tree fntype = build_function_type(TREE_TYPE(res_decl), params);

    //params
    tree params_decl = NULL_TREE;
    for (size_t i = 0; i < paramsList.size(); i++) {
        tree param = build_decl(UNKNOWN_LOCATION, PARM_DECL, get_identifier(paramsList[i].c_str()), integer_type_node);
        DECL_ARG_TYPE(param) = integer_type_node;
        params_decl = chainon(params_decl, param);
        m_LocalSymbols[paramsList[i]] = new Symbol(VARIABLE, param);
    }

    func_decl = build_decl(UNKNOWN_LOCATION, FUNCTION_DECL, get_identifier(m_FunctionIdent.c_str()), fntype);
    DECL_ARGUMENTS(func_decl) = params_decl;
    DECL_RESULT(func_decl) = res_decl;
    DECL_CONTEXT(DECL_RESULT(func_decl)) = func_decl;

    if (forwardDecl) {
        //forward declaration
        DECL_EXTERNAL(func_decl) = 1;
        if (m_IsProcedure)
            m_SymbolTable[m_FunctionIdent] = new Symbol(PROCEDURE, func_decl, true);
        else
            m_SymbolTable[m_FunctionIdent] = new Symbol(FUNCTION, func_decl, true);

        m_FunctionDecl = false;
    }

    TREE_STATIC(func_decl) = true;
    TREE_PUBLIC(func_decl) = true;

    var_decls = NULL_TREE;

    if (!m_IsProcedure)
        m_LocalSymbols[m_FunctionIdent] = new Symbol(VARIABLE, res_decl);
}

void SymbolTable::functionVarsDecl() {
    block = build_block(var_decls, NULL_TREE, NULL_TREE, NULL_TREE);
    TREE_USED(block) = true;

    if (m_IsProcedure)
        m_SymbolTable[m_FunctionIdent] = new Symbol(PROCEDURE, func_decl);
    else
        m_SymbolTable[m_FunctionIdent] = new Symbol(FUNCTION, func_decl);
}

void SymbolTable::finishFunctionDecl(tree body_stmts) {
    tree stmts = alloc_stmt_list();
    append_to_statement_list(body_stmts, &stmts);

    tree ret_smt = build1(RETURN_EXPR, void_type_node, DECL_RESULT(func_decl));
    append_to_statement_list(ret_smt, &stmts);

    tree bind = build3(BIND_EXPR, void_type_node, BLOCK_VARS(block), stmts, block);
    TREE_SIDE_EFFECTS(bind) = true;

    BLOCK_SUPERCONTEXT(block) = func_decl;

    DECL_INITIAL(func_decl) = block;
    DECL_SAVED_TREE(func_decl) = bind;

    register_global_function_declaration(func_decl);

    if (m_IsProcedure)
        m_SymbolTable[m_FunctionIdent] = new Symbol(PROCEDURE, func_decl);
    else
        m_SymbolTable[m_FunctionIdent] = new Symbol(FUNCTION, func_decl);

    //cleanup
    for (std::map<std::string, Symbol*>::iterator it = m_LocalSymbols.begin(); it != m_LocalSymbols.end(); it++)
        delete it->second;

    m_LocalSymbols.clear();

    m_FunctionDecl = false;
}

bool SymbolTable::isFunctionDecl() const {
    return m_FunctionDecl;
}

tree SymbolTable::getCurrentFunction() const {
    return func_decl;
}

Symbol * SymbolTable::getSymbol(const std::string & ident, bool hideLocal/* = false*/) {
    if (m_FunctionDecl && !hideLocal) {
        //if in function block, look for local symbols first
        if (m_LocalSymbols.count(ident) == 1)
            return m_LocalSymbols[ident];
    }

    if (m_SymbolTable.count(ident) != 1)
        return NULL;

    return m_SymbolTable[ident];
}
