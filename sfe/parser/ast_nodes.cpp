#include <exception>
#include <iostream>

#include "ast_nodes.h"

//io tree nodes

tree build_string_constant(const char* string, int isConst) {
    int length = strlen(string);
    // the string is just array of const chars so index_type and const_char_type are obviously needed, size_int calls build_int_cst with type stk_sizetype (normal representation of size in bytes) and the size is the size of the string
    tree index_type = build_index_type(size_int(length));
    tree const_char_type = isConst ? build_qualified_type(unsigned_char_type_node, TYPE_QUAL_CONST) : unsigned_char_type_node;
    // then the string type is the array of const chars with known size
    tree string_type = build_array_type(const_char_type, index_type);
    // for c/c++ not important but some languages may distinguis strings and arrays of chars viz tree.def
    TYPE_STRING_FLAG(string_type) = 1;
    // actual building of string
    tree res = build_string(length + 1, string);
    //and the builded tree is of string type
    TREE_TYPE(res) = string_type;

    return res;
}

tree build_print_integer_expr(location_t loc, tree int_expr) {

    tree string = build_string_constant((const char*) "%d\n", true);

    tree * args_vec = XNEWVEC(tree, 2);
    args_vec[0] = build1(ADDR_EXPR, build_pointer_type(TREE_TYPE(string)), string);
    args_vec[1] = int_expr;

    tree params = NULL_TREE;
    chainon(params, tree_cons(NULL_TREE, TREE_TYPE(args_vec[0]), NULL_TREE));
    chainon(params, tree_cons(NULL_TREE, TREE_TYPE(args_vec[1]), NULL_TREE));

    // function parameters
    tree param_decl = NULL_TREE;

    tree resdecl = build_decl(BUILTINS_LOCATION, RESULT_DECL, NULL_TREE, integer_type_node);
    DECL_ARTIFICIAL(resdecl) = true;
    DECL_IGNORED_P(resdecl) = true;

    tree fntype = build_function_type(TREE_TYPE(resdecl), params);
    tree fndecl = build_decl(UNKNOWN_LOCATION, FUNCTION_DECL, get_identifier("printf"), fntype);
    DECL_ARGUMENTS(fndecl) = param_decl;

    DECL_RESULT(fndecl) = resdecl;

    DECL_ARTIFICIAL(resdecl) = true;
    DECL_IGNORED_P(resdecl) = true;
    DECL_EXTERNAL(fndecl) = true;

    tree call = build_call_expr_loc_array(loc, fndecl, 2, args_vec);
    SET_EXPR_LOCATION(call, loc);
    TREE_USED(call) = true;

    return call;
}

tree build_scan_integer(location_t loc, tree var_expr) {
    tree string = build_string_constant((const char*) "%d", true);

    tree * args_vec = XNEWVEC(tree, 2);
    args_vec[0] = build1(ADDR_EXPR, build_pointer_type(TREE_TYPE(string)), string);
    args_vec[1] = build1(ADDR_EXPR, build_pointer_type(TREE_TYPE(var_expr)), var_expr);

    tree params = NULL_TREE;
    chainon(params, tree_cons(NULL_TREE, TREE_TYPE(args_vec[0]), NULL_TREE));
    chainon(params, tree_cons(NULL_TREE, TREE_TYPE(args_vec[1]), NULL_TREE));

    // function parameters
    tree param_decl = NULL_TREE;

    tree resdecl = build_decl(BUILTINS_LOCATION, RESULT_DECL, NULL_TREE, integer_type_node);
    DECL_ARTIFICIAL(resdecl) = true;
    DECL_IGNORED_P(resdecl) = true;

    tree fntype = build_function_type(TREE_TYPE(resdecl), params);
    tree fndecl = build_decl(UNKNOWN_LOCATION, FUNCTION_DECL, get_identifier("scanf"), fntype);
    DECL_ARGUMENTS(fndecl) = param_decl;

    DECL_RESULT(fndecl) = resdecl;

    DECL_ARTIFICIAL(resdecl) = true;
    DECL_IGNORED_P(resdecl) = true;
    DECL_EXTERNAL(fndecl) = true;

    tree call = build_call_expr_loc_array(loc, fndecl, 2, args_vec);
    SET_EXPR_LOCATION(call, loc);
    TREE_USED(call) = true;

    return call;
}

//atomic nodes

Variable::Variable(Symbol * symbol) : m_Symbol(symbol) {
}

bool Variable::isLValue() {
    return (m_Symbol->m_SymbolType == VARIABLE);
}

tree Variable::translate() {
    return m_Symbol->m_TreeNode;
}

ArrayRef::ArrayRef(Symbol* symbol, Expr* index) : m_Symbol(symbol), m_Index(index) {
}

ArrayRef::~ArrayRef() {
    delete m_Index;
}

tree ArrayRef::translate() {
    return build4(ARRAY_REF, integer_type_node, m_Symbol->m_TreeNode, m_Index->translate(), NULL, NULL);
}

NumLiteral::NumLiteral(int value) : m_Value(value) {
}

tree NumLiteral::translate() {
    return build_int_cst(integer_type_node, m_Value);
}

//arithmetic expressions

BinOp::BinOp(Expr * left, Expr * right) : m_Left(left), m_Right(right) {
}

BinOp::~BinOp() {
    delete m_Left;
    delete m_Right;
}

tree BinPlus::translate() {
    return build2(PLUS_EXPR, integer_type_node, m_Left->translate(), m_Right->translate());
}

tree BinMinus::translate() {
    return build2(MINUS_EXPR, integer_type_node, m_Left->translate(), m_Right->translate());
}

tree BinMultiply::translate() {
    return build2(MULT_EXPR, integer_type_node, m_Left->translate(), m_Right->translate());
}

tree BinDivide::translate() {
    return build2(TRUNC_DIV_EXPR, integer_type_node, m_Left->translate(), m_Right->translate());
}

tree BinMod::translate() {
    return build2(TRUNC_MOD_EXPR, integer_type_node, m_Left->translate(), m_Right->translate());
}

UnMinus::UnMinus(Expr * expr) : m_Expr(expr) {
}

UnMinus::~UnMinus() {
    delete m_Expr;
}

tree UnMinus::translate() {
    return build1(NEGATE_EXPR, integer_type_node, m_Expr->translate());
}

//decrement / increment

PreDecrement::PreDecrement(Expr * lvalue) : m_LValue(lvalue) {
    if (!m_LValue->isLValue())
        throw new std::invalid_argument("lvalue expected");
}

PreDecrement::~PreDecrement() {
    delete m_LValue;
}

tree PreDecrement::translate() {
    return build2(PREDECREMENT_EXPR, integer_type_node, m_LValue->translate(), build_int_cst(integer_type_node, 1));
}

PreIncrement::PreIncrement(Expr * lvalue) : m_LValue(lvalue) {
    if (!m_LValue->isLValue())
        throw new std::invalid_argument("lvalue expected");
}

PreIncrement::~PreIncrement() {
    delete m_LValue;
}

tree PreIncrement::translate() {
    return build2(PREINCREMENT_EXPR, integer_type_node, m_LValue->translate(), build_int_cst(integer_type_node, 1));
}

//conditions

tree CondTrue::translate() {
    return boolean_true_node;
}

tree CondFalse::translate() {
    return boolean_false_node;
}

CondAnd::CondAnd(Expr* left, Expr* right) : m_Left(left), m_Right(right) {
}

CondAnd::~CondAnd() {
    delete m_Left;
    delete m_Right;
}

tree CondAnd::translate() {
    return build2(TRUTH_ANDIF_EXPR, integer_type_node, m_Left->translate(), m_Right->translate());
}

CondOr::CondOr(Expr* left, Expr* right) : m_Left(left), m_Right(right) {
}

CondOr::~CondOr() {
    delete m_Left;
    delete m_Right;
}

tree CondOr::translate() {
    return build2(TRUTH_ORIF_EXPR, integer_type_node, m_Left->translate(), m_Right->translate());
}

CondRelOp::CondRelOp(TokenType opType, Expr* left, Expr* right)
: m_OpType(opType), m_Left(left), m_Right(right) {
}

CondRelOp::~CondRelOp() {
    delete m_Left;
    delete m_Right;
}

tree CondRelOp::translate() {
    switch (m_OpType) {
        case EQUAL:
            return build2(EQ_EXPR, integer_type_node, m_Left->translate(), m_Right->translate());
        case NOT_EQUAL:
            return build2(NE_EXPR, integer_type_node, m_Left->translate(), m_Right->translate());
        case LESS:
            return build2(LT_EXPR, integer_type_node, m_Left->translate(), m_Right->translate());
        case LESS_OR_EQ:
            return build2(LE_EXPR, integer_type_node, m_Left->translate(), m_Right->translate());
        case GREATER:
            return build2(GT_EXPR, integer_type_node, m_Left->translate(), m_Right->translate());
        case GREATER_OR_EQ:
            return build2(GE_EXPR, integer_type_node, m_Left->translate(), m_Right->translate());
        default:
            //should not happen
            return NULL;
    }
}

CondNot::CondNot(Expr* cond) : m_Cond(cond) {
}

CondNot::~CondNot() {
    delete m_Cond;
}

tree CondNot::translate() {
    return build1(TRUTH_NOT_EXPR, integer_type_node, m_Cond->translate());
}

//statements

EmptyStmt::EmptyStmt() {
}

tree EmptyStmt::translate() {
    return build_empty_stmt(UNKNOWN_LOCATION);
}

ExprStmt::ExprStmt(Expr* expr) : m_Expr(expr) {
}

ExprStmt::~ExprStmt() {
    delete m_Expr;
}

tree ExprStmt::translate() {
    return m_Expr->translate();
}

Assign::Assign(Expr * left, Expr * right) : m_Left(left), m_Right(right) {
    if (!m_Left->isLValue())
        throw new std::invalid_argument("left: lvalue expected");
}

Assign::~Assign() {
    delete m_Left;
    delete m_Right;
}

tree Assign::translate() {
    tree left = m_Left->translate();
    return build2(MODIFY_EXPR, TREE_TYPE(left), left, m_Right->translate());
}

ReadLn::ReadLn(Expr * lvalue) : m_LValue(lvalue) {
    if (!m_LValue->isLValue())
        throw new std::invalid_argument("lvalue expected");
}

ReadLn::~ReadLn() {
    delete m_LValue;
}

tree ReadLn::translate() {
    return build_scan_integer(UNKNOWN_LOCATION, m_LValue->translate());
}

WriteLn::WriteLn(Expr * expr) : m_Expr(expr) {
}

WriteLn::~WriteLn() {
    delete m_Expr;
}

tree WriteLn::translate() {
    return build_print_integer_expr(UNKNOWN_LOCATION, m_Expr->translate());
}

//statement list

StmtList::StmtList(Stmt * stmt, StmtList * next) : m_Stmt(stmt), m_Next(next) {
}

StmtList::~StmtList() {

    delete m_Stmt;
    delete m_Next;
}

tree StmtList::translate() {
    tree res = alloc_stmt_list();
    StmtList * s = this;
    do {
        append_to_statement_list(s->m_Stmt->translate(), &res);
        s = s->m_Next;
    } while (s);

    return res;
}

void StmtList::appendStmtList(StmtList * stmtList) {
    StmtList * s = this;
    while (s->m_Next) {
        s = s->m_Next;
    }

    s->m_Next = stmtList;
}

//if statement

IfStmt::IfStmt(Expr* cond, StmtList* thenBlock, StmtList* elseBlock)
: m_Cond(cond), m_ThenBlock(thenBlock), m_ElseBlock(elseBlock) {
}

IfStmt::~IfStmt() {
    delete m_Cond;
    delete m_ThenBlock;
    delete m_ElseBlock;
}

tree IfStmt::translate() {
    return build3(COND_EXPR, void_type_node, m_Cond->translate(), m_ThenBlock->translate(), m_ElseBlock->translate());
}

//break

BreakStmt::BreakStmt(Expr* cond) : m_Cond(cond) {
}

BreakStmt::BreakStmt() : m_Cond(new CondTrue()) {
}

BreakStmt::~BreakStmt() {
    delete m_Cond;
}

tree BreakStmt::translate() {
    return build1(EXIT_EXPR, void_type_node, m_Cond->translate());
}

//loop statements

WhileStmt::WhileStmt(Expr * cond, StmtList * block) {
    m_Block = new StmtList(new BreakStmt(new CondNot(cond)), block);
}

WhileStmt::~WhileStmt() {
    delete m_Block;
}

tree WhileStmt::translate() {
    return build1(LOOP_EXPR, void_type_node, m_Block->translate());
}

ForStmt::ForStmt(TokenType type, Symbol * var, Stmt * initStmt, Expr * to, StmtList * block) {
    Expr * cond = new CondRelOp(type == KW_TO ? LESS_OR_EQ : GREATER_OR_EQ, new Variable(var), to);
    Stmt * update;
    if (type == KW_TO)
        update = new ExprStmt(new PreIncrement(new Variable(var)));
    else
        update = new ExprStmt(new PreDecrement(new Variable(var)));
    block->appendStmtList(new StmtList(update, NULL));
    WhileStmt * loop = new WhileStmt(cond, block);
    m_Block = new StmtList(initStmt, new StmtList(loop, NULL));
}

ForStmt::~ForStmt() {
    delete m_Block;
}

tree ForStmt::translate() {
    return m_Block->translate();
}

//function call

FuncCall::FuncCall(Symbol* symbol, const std::vector<Expr *> & argsList) : m_Symbol(symbol), m_ArgsList(argsList) {
}

FuncCall::~FuncCall() {
    for (size_t i = 0; i < m_ArgsList.size(); i++)
        delete m_ArgsList[i];
}

tree FuncCall::translate() {
    tree * args_vec = XNEWVEC(tree, m_ArgsList.size());
    //arguments
    for (size_t i = 0; i < m_ArgsList.size(); i++) {
        args_vec[i] = m_ArgsList[i]->translate();
    }

    tree call = build_call_expr_loc_array(UNKNOWN_LOCATION, m_Symbol->m_TreeNode, m_ArgsList.size(), args_vec);
    SET_EXPR_LOCATION(call, UNKNOWN_LOCATION);
    TREE_USED(call) = true;
    return call;
}


//function exit

ExitStmt::ExitStmt(tree funcDecl) : m_FuncDecl(funcDecl) {
}

tree ExitStmt::translate() {
    return build1(RETURN_EXPR, void_type_node, DECL_RESULT(m_FuncDecl));
}

Prog::Prog(StmtList* mainBlock) : m_MainBlock(mainBlock) {
}

Prog::~Prog() {

    delete m_MainBlock;
}

tree Prog::translate() {
    return m_MainBlock->translate();
}
