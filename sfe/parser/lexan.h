#ifndef LEXAN_H
#define LEXAN_H

#include <istream>
#include <map>

/* token (lexical element) types */
enum TokenType {
    NUMBER, IDENT,
    PLUS, MINUS, TIMES, DIVIDED, MOD,
    L_PAR, R_PAR, L_BRACKET, R_BRACKET,
    LESS, LESS_OR_EQ, GREATER, GREATER_OR_EQ,
    ASSIGN, EQUAL, NOT_EQUAL,
    SEMICOLON, COMMA, COLON, DOT,
    KW_OR, KW_AND, KW_TRUE, KW_FALSE,
    KW_PROGRAM, KW_FUNCTION, KW_PROCEDURE, KW_EXIT, KW_FORWARD,
    KW_VAR, KW_CONST, KW_BEGIN, KW_END,
    KW_IF, KW_THEN, KW_ELSE,
    KW_WHILE, KW_FOR, KW_TO, KW_DOWNTO, KW_DO, KW_BREAK,
    KW_ARRAY, KW_OF, KW_WRITELN, KW_READLN, KW_DEC, KW_INC,
    EOI, ERROR
};

/* token (lexical element) */
struct Token {
    TokenType m_Type;
    int m_NumVal;
    std::string m_StrVal;

    static std::string tokenTypesNames[];
};

enum CharType {
    DIGIT, LETTER, WHITESPACE, UNDERSCORE, OTHER, END
};

/* lexical analyser */
class Lexan {
public:
    Lexan(std::istream & input);
    ~Lexan();

    /* reads and returns next token from input */
    Token nextToken();

    /* returns current line where we read */
    int getCurrentLine() const;

private:
    char m_Character;
    CharType m_CharType;
    std::istream & m_Input;
    int m_InputLine;

    std::map<std::string, TokenType> m_KeyWordTable;

    void readCharacter();
    void initKeyWordTable();
};

#endif /* LEXAN_H */
