#ifndef PARSER_H
#define PARSER_H

#include <exception>
#include <istream>
#include <vector>

#include "../sfe-lang.h"

#include "lexan.h"
#include "ast_nodes.h"

/* represents error during parsing (caused by invalid input), 
   throwing this exception should terminate parsing */
class ParserException : public std::runtime_error {
public:

    ParserException() : std::runtime_error("parser error") {
    }
};

/* ll parser */
class Parser {
public:
    Parser(std::istream & input);
    ~Parser();

    /* parses input, returns result as gcc generic tree */
    tree parse();

private:
    Lexan m_Lexan;
    Token m_Symbol;

    /* matches terminal symbol of given type, 
       throws ParserException if token of different type is encountered */
    Token match(TokenType t);

    //nonterminals

    //starting nonterminal
    Prog * nonterm_Prog();

    //function and procedure declarations
    void nonterm_FuncDecl();
    void nonterm_FuncParamDecl(std::vector<std::string> & paramsList);

    //variable and constant declarations
    void nonterm_Decl();
    void nonterm_VarDeclBlock();
    void nonterm_VarDecl();
    void nonterm_VarDecl2(std::vector<std::string> & symbolList);
    void nonterm_ConstDecl();
    void nonterm_DeclType(std::vector<std::string> & symbolList);

    //statements
    StmtList * nonterm_StmtBlock(bool functionBlock = false);
    StmtList * nonterm_StmtBlock2();
    Stmt * nonterm_Stmt();
    Stmt * nonterm_AssignStmt(Expr * lvalue);

    //expresions

    //logical expressions
    Expr * nonterm_Expr();
    Expr * nonterm_Expr2(Expr * c);
    Expr * nonterm_LogTerm();
    Expr * nonterm_LogTerm2(Expr * c);

    //arithmetic axpressions
    Expr * nonterm_AritExpr();
    Expr * nonterm_AritExpr2(Expr * e);
    Expr * nonterm_T();
    Expr * nonterm_T2(Expr * e);
    Expr * nonterm_F();
    Expr * nonterm_Atom(bool rvalue = true);

    //function call
    Expr * nonterm_FuncCall(Symbol * symb);
    void nonterm_FuncCallArgs(std::vector<Expr *> & argsList);


    /* checks if type is valid, throws exception if it is not*/
    void checkType(const Token & type);
};

#endif /* PARSER_H */
