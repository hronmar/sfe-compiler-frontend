#ifndef FRONTEND_H
#define FRONTEND_H

#include "../sfe-lang.h"

/* parses input file, returns result as gcc generic tree */
tree frontendParseFile(const char * fileName);

#endif /* FRONTEND_H */
