#ifndef AST_NODES_H
#define AST_NODES_H

#include "../sfe-lang.h"

#include "lexan.h"
#include "symbol_table.h"

//generic node

class Node {
public:
    virtual tree translate() = 0;

    virtual ~Node() {
    }
};

//logical / arithmetic expressions

class Expr : public Node {
public:

    /* indicates if this expression may be on left side of assignment */
    virtual bool isLValue() {
        return false;
    }
};

//atomic nodes

class Variable : public Expr {
public:
    Variable(Symbol * symbol);
    virtual tree translate();
    virtual bool isLValue();

private:
    Symbol * m_Symbol;
};

class ArrayRef : public Expr {
public:
    ArrayRef(Symbol * symbol, Expr * index);
    ~ArrayRef();
    virtual tree translate();

    virtual bool isLValue() {
        return true;
    }

private:
    Symbol * m_Symbol;
    Expr * m_Index;
};

class NumLiteral : public Expr {
public:
    NumLiteral(int value);

    virtual tree translate();

private:
    int m_Value;
};

//binary operators

class BinOp : public Expr {
public:
    BinOp(Expr * left, Expr * right);
    virtual ~BinOp();
    virtual tree translate() = 0;

protected:
    Expr * m_Left, * m_Right;
};

class BinPlus : public BinOp {
public:

    BinPlus(Expr * left, Expr * right) : BinOp(left, right) {
    }

    virtual tree translate();
};

class BinMinus : public BinOp {
public:

    BinMinus(Expr * left, Expr * right) : BinOp(left, right) {
    }

    virtual tree translate();
};

class BinMultiply : public BinOp {
public:

    BinMultiply(Expr * left, Expr * right) : BinOp(left, right) {
    }

    virtual tree translate();
};

class BinDivide : public BinOp {
public:

    BinDivide(Expr * left, Expr * right) : BinOp(left, right) {
    }

    virtual tree translate();
};

class BinMod : public BinOp {
public:

    BinMod(Expr * left, Expr * right) : BinOp(left, right) {
    }

    virtual tree translate();
};

//unary operators

class UnMinus : public Expr {
public:
    UnMinus(Expr * expr);
    virtual ~UnMinus();
    virtual tree translate();

private:
    Expr * m_Expr;
};

//decrement / increment

class PreDecrement : public Expr {
public:
    PreDecrement(Expr * lvalue);
    virtual ~PreDecrement();
    virtual tree translate();

private:
    Expr * m_LValue;
};

class PreIncrement : public Expr {
public:
    PreIncrement(Expr * lvalue);
    virtual ~PreIncrement();
    virtual tree translate();

private:
    Expr * m_LValue;
};

//conditions (logical expressions)

class CondTrue : public Expr {
public:
    virtual tree translate();
};

class CondFalse : public Expr {
public:
    virtual tree translate();
};

class CondAnd : public Expr {
public:
    CondAnd(Expr * left, Expr * right);
    virtual ~CondAnd();
    virtual tree translate();

private:
    Expr * m_Left;
    Expr * m_Right;
};

class CondOr : public Expr {
public:
    CondOr(Expr * left, Expr * right);
    virtual ~CondOr();
    virtual tree translate();

private:
    Expr * m_Left;
    Expr * m_Right;
};

class CondRelOp : public Expr {
public:
    CondRelOp(TokenType opType, Expr * left, Expr * right);
    virtual ~CondRelOp();
    virtual tree translate();

private:
    TokenType m_OpType;
    Expr * m_Left;
    Expr * m_Right;
};

class CondNot : public Expr {
public:
    CondNot(Expr * cond);
    virtual ~CondNot();
    virtual tree translate();

private:
    Expr * m_Cond;
};

//statements

class Stmt : public Node {
};

class EmptyStmt : public Stmt {
public:
    EmptyStmt();
    virtual tree translate();
};

//expression with side effect

class ExprStmt : public Stmt {
public:
    ExprStmt(Expr * expr);
    virtual ~ExprStmt();
    virtual tree translate();

private:
    Expr * m_Expr;
};

//assignment statement

class Assign : public Stmt {
public:
    Assign(Expr * left, Expr * right);
    virtual ~Assign();
    virtual tree translate();

private:
    Expr * m_Left;
    Expr * m_Right;
};

class ReadLn : public Stmt {
public:
    ReadLn(Expr * lvalue);
    virtual ~ReadLn();
    virtual tree translate();

private:
    Expr * m_LValue;
};

class WriteLn : public Stmt {
public:
    WriteLn(Expr * expr);
    virtual ~WriteLn();
    virtual tree translate();

private:
    Expr * m_Expr;
};

//statement list

class StmtList : public Node {
public:
    StmtList(Stmt * stmt, StmtList * next);
    virtual ~StmtList();
    virtual tree translate();
    void appendStmtList(StmtList * stmtList);

private:
    Stmt * m_Stmt;
    StmtList * m_Next;
};

//conditional statement

class IfStmt : public Stmt {
public:
    IfStmt(Expr * cond, StmtList * thenBlock, StmtList * elseBlock);
    virtual ~IfStmt();
    virtual tree translate();

private:
    Expr * m_Cond;
    StmtList * m_ThenBlock;
    StmtList * m_ElseBlock;
};

//break

class BreakStmt : public Stmt {
public:
    BreakStmt();
    BreakStmt(Expr * cond);
    virtual ~BreakStmt();
    virtual tree translate();

private:
    Expr * m_Cond;
};

//loop statements

class WhileStmt : public Stmt {
public:
    WhileStmt(Expr * cond, StmtList * m_Block);
    virtual ~WhileStmt();
    virtual tree translate();

private:
    StmtList * m_Block;
};

class ForStmt : public Stmt {
public:
    ForStmt(TokenType type, Symbol * var, Stmt * initStmt, Expr * to, StmtList * block);
    virtual ~ForStmt();
    virtual tree translate();

private:
    StmtList * m_Block;
};

//function call

class FuncCall : public Expr {
public:
    FuncCall(Symbol * symbol, const std::vector<Expr *> & argsList);
    ~FuncCall();
    virtual tree translate();

private:
    Symbol * m_Symbol;
    std::vector<Expr *> m_ArgsList;
};

//function exit

class ExitStmt : public Stmt {
public:
    ExitStmt(tree funcDecl);
    virtual tree translate();

private:
    tree m_FuncDecl;
};

//program node

class Prog : public Node {
public:
    Prog(StmtList * mainBlock);
    virtual ~Prog();
    virtual tree translate();

private:
    StmtList * m_MainBlock;
};

#endif /* AST_NODES_H */
