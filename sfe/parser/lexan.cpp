#include <iostream>
#include <cstdio>

#include "lexan.h"

std::string Token::tokenTypesNames[] = {"Number", "Identifier",
    "PLUS", "MINUS", "TIMES", "DIVIDED", "MOD",
    "L_PAR", "R_PAR", "L_BRACKET", "R_BRACKET",
    "LESS", "LESS_OR_EQ", "GREATER", "GREATER_OR_EQ",
    "ASSIGN", "EQUAL", "NOT_EQUAL",
    "SEMICOLON", "COMMA", "COLON", "DOT",
    "KW_OR", "KW_AND", "KW_TRUE", "KW_FALSE",
    "KW_PROGRAM", "KW_FUNCTION", "KW_PROCEDURE", "KW_EXIT", "KW_FORWARD",
    "KW_VAR", "KW_CONST", "KW_BEGIN", "KW_END",
    "KW_IF", "KW_THEN", "KW_ELSE",
    "KW_WHILE", "KW_FOR", "KW_TO", "KW_DOWNTO", "KW_DO", "KW_BREAK",
    "KW_ARRAY", "KW_OF", "KW_WRITELN", "KW_READLN", "KW_DEC", "KW_INC",
    "EOI", "ERROR"};

Lexan::Lexan(std::istream & input) : m_Character(EOF), m_Input(input), m_InputLine(1) {
    initKeyWordTable();
    m_Input >> std::noskipws;
    readCharacter();
}

Lexan::~Lexan() {
}

Token Lexan::nextToken() {
    Token token;
    int base = 10;

qInit:
    switch (m_CharType) {
        case WHITESPACE:
            readCharacter();
            goto qInit;
        case LETTER:
            token.m_Type = IDENT;
            token.m_StrVal = "";
            token.m_StrVal += m_Character;
            readCharacter();
            goto qIdent;
        case DIGIT:
            token.m_Type = NUMBER;
            base = 10;
            token.m_NumVal = m_Character - '0';
            readCharacter();
            goto qNum;
        default:
            ;
    }

    //operators, special characters
    switch (m_Character) {
        case '+':
            token.m_Type = PLUS;
            readCharacter();
            return token;
        case '-':
            token.m_Type = MINUS;
            readCharacter();
            return token;
        case '*':
            token.m_Type = TIMES;
            readCharacter();
            return token;
        case '(':
            token.m_Type = L_PAR;
            readCharacter();
            return token;
        case ')':
            token.m_Type = R_PAR;
            readCharacter();
            return token;
        case '[':
            token.m_Type = L_BRACKET;
            readCharacter();
            return token;
        case ']':
            token.m_Type = R_BRACKET;
            readCharacter();
            return token;
        case '<':
            token.m_Type = LESS;
            readCharacter();
            goto qLess;
        case '>':
            token.m_Type = GREATER;
            readCharacter();
            goto qGreater;
        case '=':
            token.m_Type = EQUAL;
            readCharacter();
            return token;
        case ':':
            token.m_Type = COLON;
            readCharacter();
            goto qColon;
        case ';':
            token.m_Type = SEMICOLON;
            readCharacter();
            return token;
        case ',':
            token.m_Type = COMMA;
            readCharacter();
            return token;
        case '.':
            token.m_Type = DOT;
            readCharacter();
            return token;
        case '$':
            token.m_Type = NUMBER;
            base = 16;
            token.m_NumVal = 0;
            readCharacter();
            goto qNumBase;
        case '&':
            token.m_Type = NUMBER;
            base = 8;
            token.m_NumVal = 0;
            readCharacter();
            goto qNumBase;
        default:
            if (m_CharType == END) {
                token.m_Type = EOI;
                readCharacter();
                return token;
            }

            std::cout << "Lexan error: invalid character!" << std::endl;
            token.m_Type = ERROR;
            readCharacter();
            return token;
    }

qLess:
    switch (m_Character) {
        case '=':
            token.m_Type = LESS_OR_EQ;
            readCharacter();
            return token;
        case '>':
            token.m_Type = NOT_EQUAL;
            readCharacter();
            return token;
        default:
            return token;
    }

qGreater:
    switch (m_Character) {
        case '=':
            token.m_Type = GREATER_OR_EQ;
            readCharacter();
            return token;
        default:
            return token;
    }

qColon:
    switch (m_Character) {
        case '=':
            token.m_Type = ASSIGN;
            readCharacter();
            return token;
        default:
            return token;
    }

qIdent:
    switch (m_CharType) {
        case LETTER:
        case DIGIT:
        case UNDERSCORE:
            token.m_StrVal += m_Character;
            readCharacter();
            goto qIdent;
        default:
            try {
                //if it is a keyword, change token type
                token.m_Type = m_KeyWordTable.at(token.m_StrVal);
            } catch (std::out_of_range e) {
            }
            return token;
    }

qNum:
    switch (m_CharType) {
        case DIGIT:
        case LETTER:
        {
            int val;

            //compute digit value
            if (m_CharType == LETTER)
                val = tolower(m_Character) - 'a' + 10;
            else
                val = m_Character - '0';

            if (val >= base) {
                std::cout << "Lexan error: invalid digit for current base!" << std::endl;
                token.m_Type = ERROR;
                readCharacter();
                return token;
            }

            token.m_NumVal *= base;
            token.m_NumVal += val;
            readCharacter();
            goto qNum;
        }
        default:
            return token;
    }

qNumBase:
    switch (m_CharType) {
        case DIGIT:
        case LETTER:
            goto qNum;
        default:
            std::cout << "Lexan error: digit expected!" << std::endl;
            token.m_Type = ERROR;
            readCharacter();
            return token;
    }

}

int Lexan::getCurrentLine() const {
    return m_InputLine;
}

void Lexan::readCharacter() {
    if (!(m_Input >> m_Character)) {
        m_CharType = END;
        return;
    }

    if (isalpha(m_Character))
        m_CharType = LETTER;
    else if (isdigit(m_Character))
        m_CharType = DIGIT;
    else if (m_Character == EOF)
        m_CharType = END;
    else if (m_Character == '_')
        m_CharType = UNDERSCORE;
    else if (m_Character <= ' ')
        m_CharType = WHITESPACE;
    else
        m_CharType = OTHER;

    if (m_Character == '\n')
        m_InputLine++;
}

void Lexan::initKeyWordTable() {
    m_KeyWordTable["div"] = DIVIDED;
    m_KeyWordTable["mod"] = MOD;

    m_KeyWordTable["or"] = KW_OR;
    m_KeyWordTable["and"] = KW_AND;
    m_KeyWordTable["true"] = KW_TRUE;
    m_KeyWordTable["false"] = KW_FALSE;
    m_KeyWordTable["program"] = KW_PROGRAM;
    m_KeyWordTable["function"] = KW_FUNCTION;
    m_KeyWordTable["procedure"] = KW_PROCEDURE;
    m_KeyWordTable["exit"] = KW_EXIT;
    m_KeyWordTable["forward"] = KW_FORWARD;
    m_KeyWordTable["var"] = KW_VAR;
    m_KeyWordTable["const"] = KW_CONST;
    m_KeyWordTable["begin"] = KW_BEGIN;
    m_KeyWordTable["end"] = KW_END;
    m_KeyWordTable["if"] = KW_IF;
    m_KeyWordTable["then"] = KW_THEN;
    m_KeyWordTable["else"] = KW_ELSE;
    m_KeyWordTable["while"] = KW_WHILE;
    m_KeyWordTable["for"] = KW_FOR;
    m_KeyWordTable["to"] = KW_TO;
    m_KeyWordTable["downto"] = KW_DOWNTO;
    m_KeyWordTable["do"] = KW_DO;
    m_KeyWordTable["break"] = KW_BREAK;
    m_KeyWordTable["array"] = KW_ARRAY;
    m_KeyWordTable["of"] = KW_OF;
    m_KeyWordTable["writeln"] = KW_WRITELN;
    m_KeyWordTable["readln"] = KW_READLN;
    m_KeyWordTable["dec"] = KW_DEC;
    m_KeyWordTable["inc"] = KW_INC;
}
